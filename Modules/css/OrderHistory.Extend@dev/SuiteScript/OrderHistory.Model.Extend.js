/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

// OrderHistory.Model.js
// ----------
// Handles fetching orders
define(
	'OrderHistory.Model.Extend'
,	[	'OrderHistory.Model'
	,	'Application'
	,	'Utils'
	,	'SC.Model'
	,	'StoreItem.Model'
	,	'ReturnAuthorization.Model'
	,	'Receipt.List'

	,	'underscore'
	]
,	function (
		originalModel
	,	Application
	,	Utils
	,	SCModel
	,	StoreItem
	,	ReturnAuthorization
	,	ReceiptList

	,	_
	)
{
	'use strict';

	return _.extend(originalModel, {
	
		createResult: function (placed_order)
		{
			return {
				internalid: placed_order.getId()
			,	type: placed_order.getRecordType()
			,	trantype: placed_order.getFieldValue('type')
			,	order_number: placed_order.getFieldValue('tranid')
			,	purchasenumber: placed_order.getFieldValue('otherrefnum')
			,	dueDate: placed_order.getFieldValue('duedate')
			,	amountDue: Utils.toCurrency(placed_order.getFieldValue('amountremainingtotalbox'))
			,	amountDue_formatted: Utils.formatCurrency(placed_order.getFieldValue('amountremainingtotalbox'))
			,	memo: placed_order.getFieldValue('memo')
			,   date: placed_order.getFieldValue('trandate')
			,   status: placed_order.getFieldValue('status')
			,   comments: placed_order.getFieldValue('custbody_shipping_notes') || 'Not specified'
			,	isReturnable: this.isReturnable(placed_order)
			,	summary: {
					subtotal: Utils.toCurrency(placed_order.getFieldValue('subtotal'))
				,	subtotal_formatted: Utils.formatCurrency(placed_order.getFieldValue('subtotal'))

				,	taxtotal: Utils.toCurrency(placed_order.getFieldValue('taxtotal'))
				,	taxtotal_formatted: Utils.formatCurrency(placed_order.getFieldValue('taxtotal'))

				,	tax2total: Utils.toCurrency(0)
				,	tax2total_formatted: Utils.formatCurrency(0)

				,	shippingcost: Utils.toCurrency(placed_order.getFieldValue('shippingcost'))
				,	shippingcost_formatted: Utils.formatCurrency(placed_order.getFieldValue('shippingcost'))

				,	handlingcost: Utils.toCurrency(placed_order.getFieldValue('althandlingcost'))
				,	handlingcost_formatted: Utils.formatCurrency(placed_order.getFieldValue('althandlingcost'))

				,	estimatedshipping: 0
				,	estimatedshipping_formatted: Utils.formatCurrency(0)

				,	taxonshipping: Utils.toCurrency(0)
				,	taxonshipping_formatted: Utils.formatCurrency(0)

				,	discounttotal: Utils.toCurrency(placed_order.getFieldValue('discounttotal'))
				,	discounttotal_formatted: Utils.formatCurrency(placed_order.getFieldValue('discounttotal'))

				,	taxondiscount: Utils.toCurrency(0)
				,	taxondiscount_formatted: Utils.formatCurrency(0)

				,	discountrate: Utils.toCurrency(0)
				,	discountrate_formatted: Utils.formatCurrency(0)

				,	discountedsubtotal: Utils.toCurrency(0)
				,	discountedsubtotal_formatted: Utils.formatCurrency(0)

				,	giftcertapplied: Utils.toCurrency(placed_order.getFieldValue('giftcertapplied'))
				,	giftcertapplied_formatted: Utils.formatCurrency(placed_order.getFieldValue('giftcertapplied'))

				,	total: Utils.toCurrency(placed_order.getFieldValue('total'))
				,	total_formatted: Utils.formatCurrency(placed_order.getFieldValue('total'))
				}

			,	currency: context.getFeature('MULTICURRENCY') ?
				{
					internalid: placed_order.getFieldValue('currency')
				,	name: placed_order.getFieldValue('currencyname')
				} : null
			};
		}
	});
});