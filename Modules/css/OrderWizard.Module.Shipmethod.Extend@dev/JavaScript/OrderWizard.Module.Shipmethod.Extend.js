/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module OrderWizard.Module.Shipmethod
define(
	'OrderWizard.Module.Shipmethod.Extend'
,	[	
		'OrderWizard.Module.Shipmethod'
	,	'Wizard.Module'
	,	'Profile.Model'
	,	'GlobalViews.Message.View'

	,	'order_wizard_shipmethod_module.tpl'

	,	'underscore'
	,	'jQuery'
	]
,	function (
		OrderWizardModuleShipmethod
	,	WizardModule
	,	ProfileModel
	,	GlobalViewsMessageView

	,	order_wizard_shipmethod_module_tpl

	,	_
	,	jQuery
	)
{
	'use strict';
	//@class OrderWizard.Module.Shipmethod @extends Wizard.Module
	return _.extend(OrderWizardModuleShipmethod.prototype,{

		//@property {Object} events
		//@method submit
		submit: function ()
		{
			var options = this.model.get("options");
			options.custbody_shipping_notes = jQuery("#shippingnotes").val();
			this.model.set("options",options)
			return this.isValid();
		}
		//@method isValid

		//@method getContext @returns OrderWizard.Module.Shipmethod.Context
	,	getContext: function ()
		{

			var self = this
			,	show_enter_shipping_address_first = !this.model.get('isEstimating') && !this.profileModel.get('addresses').get(this.model.get('shipaddress'))
			,	shipping_methods = this.model.get('shipmethods').map(function (shipmethod)
				{
					return {
							name: shipmethod.get('name')
						,	rate_formatted: shipmethod.get('rate_formatted')
						,	internalid: shipmethod.get('internalid')
						,	isActive: shipmethod.get('internalid') === self.model.get('shipmethod')
					};
				});

			//@class OrderWizard.Module.Shipmethod.Context
			return {
					//@property {LiveOrder.Model} model
					model: this.model
					//@property {Boolean} showEnterShippingAddressFirst
				,	showEnterShippingAddressFirst: show_enter_shipping_address_first
					//@property {Boolean} showLoadingMethods
				,	showLoadingMethods: this.reloadingMethods
					//@property {Boolean} hasShippingMethods
				,	hasShippingMethods: !!shipping_methods.length
					//@property {Boolean} display select instead of radio buttons
				,	showSelectForShippingMethod: shipping_methods.length > 5
					//@property {Array} shippingMethods
				,	shippingMethods: shipping_methods
					//@property {Boolean} showTitle
				,	showTitle: !this.options.hide_title
					//@property {Straing} title
				,	title: this.options.title || _('Delivery Method').translate()
				,	comments: this.model.get("options").custbody_shipping_notes
			};
		}
	});
});
