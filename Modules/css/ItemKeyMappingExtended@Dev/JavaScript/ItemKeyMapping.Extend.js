define('ItemKeyMapping.Extend', [
    'underscore',
    'Utils',
    'UrlHelper',
    'SC.Configuration'
], function ItemKeyMappingItemURL(
    _,
    Utils,
    UrlHelper,
    Configuration
) {
    'use strict';
    // Checked
    function itemImageFlatten (images)
    {
        if ('url' in images && 'altimagetext' in images)
        {
        return [images];
        }

        return _.flatten(_.map(images, function (item)
        {
        if (_.isArray(item))
        {
        return item;
        }

        return itemImageFlatten(item);
        }));
    }
    function getKeyMappers(application) {
        return {
              _thumbnail: function (item)
                {

                    var matrix_item = _.find(item.get('matrix_parent').matrixchilditems_detail,{itemid:item.get("itemid")}); 

                    

                    var item_images_detail = item.get('itemimages_detail') || {};


                    // If you generate a thumbnail position in the itemimages_detail it will be used
                    if (item_images_detail.thumbnail)
                    {
                        if (_.isArray(item_images_detail.thumbnail.urls) && item_images_detail.thumbnail.urls.length)
                        {
                            return item_images_detail.thumbnail.urls[0];
                        }

                        return item_images_detail.thumbnail;
                    }

                    // otherwise it will try to use the storedisplaythumbnail
                    if (SC.ENVIRONMENT.siteType && SC.ENVIRONMENT.siteType === 'STANDARD' && item.get('storedisplaythumbnail'))
                    {
                        return {
                            url: item.get('storedisplaythumbnail')
                            ,   altimagetext: item.get('_name')
                        };
                    }
                    // No images huh? carry on

                    var parent_item = item.get('_matrixParent');

                    if (parent_item && item.get('custitem_child_image')){
                        var home_touchpoint = Configuration.get('siteSettings.touchpoints').home,
                            parser = document.createElement('a');
                        parser.href = home_touchpoint
                        var home_url = parser.hostname;

                        return {
                            url: "http://" + home_url + item.get('custitem_child_image')
                            ,   altimagetext: item.get('_name')
                        }
                    }
                    // If the item is a matrix child, it will return the thumbnail of the parent
                    if (parent_item && parent_item.get('internalid'))
                    {
                        return parent_item.get('_thumbnail');
                    }

                    var images = itemImageFlatten(item_images_detail);
                    // If you using the advance images features it will grab the 1st one
                    if (images.length)
                    {
                        return images[0];
                    }

                    // still nothing? image the not available
                    return {
                        url: Configuration.imageNotAvailable
                        ,   altimagetext: item.get('_name')
                    };
                }
           
        };
    }

    return {
        mountToApp: function mountToApp(application) {
                Configuration.itemKeyMapping = Configuration.itemKeyMapping || {};
                _.extend(Configuration.itemKeyMapping, getKeyMappers(application));
        }
    };
});
