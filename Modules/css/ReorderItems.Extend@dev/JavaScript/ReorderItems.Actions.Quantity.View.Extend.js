/*
	© 2015 NetSuite Inc.
	User may not copy, modify, distribute, or re-bundle or otherwise make available this code;
	provided, however, if you are an authorized user with a NetSuite account or log-in, you
	may use this code subject to the terms that govern your access and use.
*/

//@module ReorderItems
define(
	'ReorderItems.Actions.Quantity.View.Extend'
,	[	'ReorderItems.Actions.Quantity.View'

	,	'Backbone'
	,	'jQuery'
	,	'underscore'
	]
,	function (
		ReorderItemsActionsQuantityView
	
	,	Backbone
	,	jQuery
	,	_
	)
{
	'use strict';
	//@class ReorderItems.Actions.Quantity.View @extends Backbone.View

	return _.extend(ReorderItemsActionsQuantityView.prototype,{
	addQuantity: function (e)
		{
			e.preventDefault();
			
			var $element = jQuery(e.target)
			,	oldValue = $element.parent().find('input').val()
			,	newVal = parseFloat(oldValue) + 1;
					
			//From CSS training

			var customer;
			this.application.getUser().done(function(data){ customer=data });

			var loggedin = customer.get('isLoggedIn');
			

			if (loggedin == 'F')
			{
				
				multiplier = 2;

			}else{

				var multiplier = customer.get('custentity_quantity_multiplier');

				multiplier = multiplier ? parseInt(multiplier) : 2;
			
			}

			if (newVal % multiplier != 0)
			{	
			//	alert('wrong');
				this.showError(_('Items should be in multiple of 2.').translate());
			//	line.set('quantity', oldValue);
			
			}

			$element.parent().find('input').val(newVal);
		}
	});

});
