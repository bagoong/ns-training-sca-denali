define('Testimonials.Model'
, [
    'SC.Model'
  , 'Application'
  , 'Utils'
  , 'underscore'
  ]
, function TestimonialsModel(
    SCModel
  , Application
  , Utils
  , _
)
{
  'use strict';

  return SCModel.extend({

    name: 'Testimonial'

  , validation: {
      writerName: {
        required: true
      , rangeLength: [2, 50]
      },
      title: {
        required: true
      , rangeLength: [2, 200]
      },
      text: {
        required: true
      , rangeLength: [2, 999]
      },
      rating: {
        required: true
      , range: [1, 5]
      }
    }

  , create: function create(data) {

      console.log('Testimonials model', 'create');

      var record = nlapiCreateRecord('customrecord_testimonial');

      this.validate(data);

      if (session.isLoggedIn2()) {
        record.setFieldValue('custrecord_t_entity', nlapiGetUser() + '');
      }
      data.writerName && record.setFieldValue('custrecord_t_entity_name', this.sanitize(data.writerName));
      data.title && record.setFieldValue('name', this.sanitize(data.title));
      data.text && record.setFieldValue('custrecord_t_text', this.sanitize(data.text));
      data.rating && record.setFieldValue('custrecord_t_rating', parseInt(data.rating, 10));


      return nlapiSubmitRecord(record);

      /*Error encountered

      "UNEXPECTED_ERROR"
      errorMessage
      :
      "TypeError: Cannot find function sanitizeString in object [object Object]. (ssp_libraries.js#7746)"
      errorStatusCode
      :
      "500"

      changed the sanitize to below becase of having error above.

      */

    }

      // @method sanitize
  		// Escapes HTML code
  		// @param {String} text text containing HTML
  		// @returns {String} with escaped HTML
  ,	sanitize: function (text){
  			return text ? text.replace(/<br>/g, '\n').replace(/</g, '&lt;').replace(/\>/g, '&gt;') : '';
  		}


  });
});
