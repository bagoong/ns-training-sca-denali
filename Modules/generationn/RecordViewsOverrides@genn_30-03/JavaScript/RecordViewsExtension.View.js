define(
	'RecordViewsExtension.View'
,	[
		'RecordViews.View'
    ,	'underscore'
	]
,	function (
		RecordViewsView
	,	_
	)
{
	'use strict';

    return {
        mountToApp: function (application){
            RecordViewsView.prototype.getContext = function() {
                return {
    				//@property {Backbone.Model} model
    				model: this.model
    				//@property {String} id
    			,	id: this.model.id
    				//@property {Boolean} isNavigable
    			,	isNavigable: _.isBoolean(this.model.get('isNavigable')) ? this.model.get('isNavigable') : true
    				//@property {Boolean} showInModal
    			,	showInModal: _.isBoolean(this.model.get('showInModal')) ? this.model.get('showInModal') : false
    				//@property {String} touchpoint
    			,	touchpoint: this.model.get('touchpoint') || 'customercenter'
    				//@property {String} detailsURL
    			,	detailsURL: this.model.get('detailsURL')
    				//@property {String} title
    			,	title: this.model.get('title')

    			// For compatibility with existing content using RecordViews, set to false if
    			// model property not set (check for Boolean works fine)
    			,	generateRemoveButton: _.isBoolean(this.model.get('generateRemoveButton')) ?
    			                           	this.model.get('generateRemoveButton') : false

    				//@property {Array<RecordViews.View.Column>} columns
    			,	columns: this.normalizeColumns()
    			};
            }
        }
    };
});
