define('Testimonials'
, [
    'Testimonials.Router'
  ]
, function (
    Router
  )
{
  'use strict';

  return {

    mountToApp: function(application) {
      return new Router(application);
    }

  }
});