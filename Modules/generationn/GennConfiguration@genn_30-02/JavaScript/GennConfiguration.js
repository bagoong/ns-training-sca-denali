define(
    'GennConfiguration'
,   ['SC.Shopping.Configuration']
,   function (
    )
{
    'use strict';

    return	{

        mountToApp: function (application)
        {
            // reference to SC.Application('Shopping').Configuration
            var config = application.getConfig();

            config.newShoppingProperty = 'xyz123';

            // add new entry to Show dropdown
            config.resultsPerPage.push({
                items: 9,
                name: _('Show $(0) products per page').translate('9')
            });
        }
    };
});
