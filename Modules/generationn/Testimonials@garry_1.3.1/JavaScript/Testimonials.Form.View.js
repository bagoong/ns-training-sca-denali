define('Testimonials.Form.View'
, [
    'testimonials_form.tpl'
  , 'Backbone'
  , 'Backbone.FormView'
  , 'jQuery'
  , 'underscore'
  , 'GlobalViews.Message.View'
  , 'GlobalViews.StarRating.View'
  , 'Backbone.CompositeView'
  ]
, function TestimonialsFormView(
    testimonialsFormTpl
  , Backbone
  , BackboneFormView
  , jQuery
  , _
  , GlobalViewsMessageView
  , GlobalViewsStarRatingView
  , BackboneCompositeView
  )
{
  'use strict';

  return Backbone.View.extend({

    showSuccessMessage: false
    
  , template: testimonialsFormTpl

  , title: _('New Testimonial').translate()

  , bindings: {
      '[name="text"]': 'text'
    , '[name="title"]': 'title'
    , '[name="writerName"]': 'writerName'
  //  , '[name="rating"]': 'rating'
    }

  , events: {
      'submit form': 'saveForm'
    , 'rate [data-toggle="rater"]': 'rate'
    }

  , initialize: function initialize() {
      this.model.on('sync', jQuery.proxy(this, 'showSuccess'));
      this.initializeModel();
      BackboneCompositeView.add(this);
      BackboneFormView.add(this);
    }

  , getBreadcrumbPages: function getBreadcrumbPages() {
      return [{
        text: this.title
      , href: '/testimonials/new'
      }];
    }
    /*
  , showSuccess: function showSuccess() {
      // alert('Ang lufet mo!');
      alert(this.successMessage);
      this.model.clear();
      this.render();
  }
  */

  , childViews: {
      'Testimonial.StarRating': function TestimonialStarRating() {
        return new GlobalViewsStarRatingView({
          showRatingCount: false
        , isWritable: true
        , value: this.model.get('rating')
        , label: 'Rating'
        , name: 'rating'
        });
      }
    }

  , showContent: function showContent() {
      var self = this;
      this.options.application.getLayout().showContent(this).done(
        function afterShowContent() {
          self.$('[data-toggle="rater"]').rater();
        }
      );
    }

  , initializeModel: function initializeModel() {
      this.model.clear().set('rating', 0);
    }

  , rate: function rate(e, rater) {
      this.model.set(rater.name, rater.value);
    }

  , showSuccess: function showSuccess() {
      this.initializeModel();
      this.showContent();
      this.showConfirmationMessage(this.successMessage, true);
    }

  , successMessage: _('Thank you for your testimonial. We\'ll review and publish it soon.').translate()

  });
});
