define('Testimonials.Model'
, [
    'SC.Model'
  , 'Application'
  , 'Utils'
  , 'underscore'
  ]
, function TestimonialsModel(
    SCModel
  , Application
  , Utils
  , _
)
{
  'use strict';

  return SCModel.extend({

    name: 'Testimonial'

  , validation: {
      writerName: {
        required: true
      , rangeLength: [2, 50]
      },
      title: {
        required: true
      , rangeLength: [2, 200]
      },
      text: {
        required: true
      , rangeLength: [2, 999]
      },
      rating: {
        required: true
      , range: [1, 5]
      }
    }

  , create: function create(data) {

      console.log('Testimonials model', 'create');

      var record = nlapiCreateRecord('customrecord_testimonial');

      this.validate(data);

      if (session.isLoggedIn2()) {
        record.setFieldValue('custrecord_t_entity', nlapiGetUser() + '');
      }
      data.writerName && record.setFieldValue('custrecord_t_entity_name', this.sanitize(data.writerName));
      data.title && record.setFieldValue('name', this.sanitize(data.title));
      data.text && record.setFieldValue('custrecord_t_text', this.sanitize(data.text));
      data.rating && record.setFieldValue('custrecord_t_rating', parseInt(data.rating, 10));


      return nlapiSubmitRecord(record);

      /*Error encountered

      "UNEXPECTED_ERROR"
      errorMessage
      :
      "TypeError: Cannot find function sanitizeString in object [object Object]. (ssp_libraries.js#7746)"
      errorStatusCode
      :
      "500"

      changed the sanitize to below becase of having error above.

      */

    }

      // @method sanitize
  		// Escapes HTML code
  		// @param {String} text text containing HTML
  		// @returns {String} with escaped HTML

  , list: function list() {
    var paginatedSearchResults;

    var filters = [
      new nlobjSearchFilter('custrecord_t_status', null, 'is', '2')
    , new nlobjSearchFilter('isinactive', null, 'is', 'F')
    ];

    var columns = [
      new nlobjSearchColumn('name')
    , new nlobjSearchColumn('custrecord_t_rating')
    , new nlobjSearchColumn('custrecord_t_entity_name')
    , new nlobjSearchColumn('custrecord_t_text')
    , new nlobjSearchColumn('custrecord_t_creation_date')
    ];

    paginatedSearchResults = Application.getPaginatedSearchResults({
      results_per_page: 20
    , columns: columns
    , filters: filters
    , record_type: 'customrecord_testimonial'
    });

    if (paginatedSearchResults.records && paginatedSearchResults.records.length > 0) {
      paginatedSearchResults.records = _.map(paginatedSearchResults.records, function mapRecord(record) {
         return {
           title: record.getValue('name'),
           text: record.getValue('custrecord_t_text'),
           createdDate: record.getValue('custrecord_t_creation_date'),
           rating: record.getValue('custrecord_t_rating'),
           writerName: record.getValue('custrecord_t_entity_name')
         };
      });
    }
    return paginatedSearchResults;
  }

  ,	sanitize: function (text){
  			return text ? text.replace(/<br>/g, '\n').replace(/</g, '&lt;').replace(/\>/g, '&gt;') : '';
  		}


  });
});
